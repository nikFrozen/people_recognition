# ИИ по определению пола


## Запуск проекта

* Добавить в /etc/hosts  127.0.0.1	recognition.localhost.com
```sh
sudo nano /etc/hosts
```

* Подключить nginx конфиг
** перейти в папку с nginx конфигами
```sh
cd /etc/nginx/sites-enabled
```
** создать ссылку на nginx конфиг в проекте
```sh
sudo ln -s ~/Projects/people_recognition/nginx.cfg rating_nginx.cfg
```
* Перейти в папку с проектом
```sh
cd ~/Projects/people_recognition
```
* Собрать проект
```sh
make build
```

* Зупуск проекта
```sh
make start
```

* Остановка проекта
```sh
make stop
```
перейти в браузере по `recognition.localhost.com`
